package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println(GetVersion())
}

func CreateTmpFile() {
  f, _ := os.Create("tanuki.tmp")
  defer f.Close()

  os.Chmod("tanuki.tmp", 0777)
}
